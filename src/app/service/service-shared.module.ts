
import { NgModule, ModuleWithProviders } from '@angular/core';
import {UserInitService} from './userinit.service'
import {AppStoreService} from './app-store.service'
import {LocalStorageService} from './local-storage.service'


@NgModule({})
export class ServiceSharedModule {
  static forRoot(): ModuleWithProviders <ServiceSharedModule> {
    return {
    ngModule: ServiceSharedModule,
    providers: [
      UserInitService,
      AppStoreService,
      LocalStorageService
    ]
    };
}
}
