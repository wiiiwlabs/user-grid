import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store';

import { storeUserAction} from 'src/app/store/actions/user.actions';
import {User} from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class AppStoreService {

constructor(private store: Store<State>) { }

//сохраним пользователя в NgRx сторе
saveUser(user: User) {
  this.store.dispatch(storeUserAction({appuser: user}))
}

}
