import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageMap } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor(private localstorage: StorageMap) {}

  // читаем из LocalStorage
  LocalStorageGet(key: string): Observable<any> {
    return this.localstorage.get(key);
  }

  // пишем в LocalStorage
  LocalStorageSave(key: string, value: any) {
    return this.localstorage.set(key, value).subscribe((data) => {
      return data;
    });
  }
}
