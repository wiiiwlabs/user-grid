/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserInitService } from './userinit.service';

describe('Service: UserInit', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserInitService]
    });
  });

  it('should ...', inject([UserInitService], (service: UserInitService) => {
    expect(service).toBeTruthy();
  }));
});
