import { Injectable } from '@angular/core';
import {User} from 'src/app/models/user'

@Injectable({
  providedIn: 'root'
})
export class UserInitService {

  private userInitJson:User[]  = [
    {"id":1,  "name":  "Лидия", "midname":"Артуровна", "lastname":"Кондрашова"},
    {"id":2,  "name":  "Мария", "midname":"Робертовна", "lastname":"Кудряшова"},
    {"id":3,  "name":  "Дмитрий", "midname":"Михайлович", "lastname":"Новиков"},
    {"id":4,  "name":  "Ярослав", "midname":"Тимофеевич", "lastname":"Антонов"},
    {"id":5,  "name":  "Алёна", "midname":"Максимовна", "lastname":"Кузнецова"},
    {"id":6,  "name":  "Роман", "midname":"Матвеевич", "lastname":"Афанасьев"},
    {"id":7,  "name":  "Роман", "midname":"Артёмович", "lastname":"Черных"},
    {"id":8,  "name":  "Марк", "midname":"Юрьевич", "lastname":"Ефремов"},
    {"id":9,  "name":  "Максим", "midname":"Андреевич", "lastname":"Никонов"},
    {"id":10, "name":  "Екатерина", "midname":"Данииловна", "lastname":"Баранова"}
  ]

constructor() { }

  // вернем json массив 
  getInitUsers():User[] {
    return this.userInitJson; // this.http.get<User[]>("assets/users.json");
  }

  // вернем первую строку
  getFirstUser():User {
    return this.userInitJson[0];
  }


}
