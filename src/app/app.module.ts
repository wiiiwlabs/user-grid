import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ShorFioPipe } from 'src/app/ui/pipes/shortFio.pipe';

import { StoreSharedModule } from 'src/app/store/store-shared.module';
import { ServiceSharedModule } from 'src/app/service';
@NgModule({
  declarations: [AppComponent, ShorFioPipe],
  imports: [
    BrowserModule,
    AppRoutingModule,

    StoreSharedModule.forRoot(),
    ServiceSharedModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
