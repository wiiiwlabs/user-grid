import { State } from '..';
import { createSelector } from '@ngrx/store';

export const selectUserState = (state: State) => state.appuser;

export const getUserSelector = createSelector(
  selectUserState,
  appuser => appuser
);


