import { NgModule, ModuleWithProviders } from '@angular/core';
//import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers, metaReducers } from 'src/app/store';
import { UserEffects } from 'src/app/store/effects/user.effects';
import { StorageModule } from '@ngx-pwa/local-storage';

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),

    EffectsModule.forRoot([UserEffects]),
    StorageModule.forRoot({ IDBNoWrap: true }),
  ],
})
export class StoreSharedModule {
  static forRoot(): ModuleWithProviders<StoreSharedModule> {
    return {
      ngModule: StoreSharedModule,
      providers: [],
    };
  }
}
