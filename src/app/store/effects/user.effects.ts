import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType , } from '@ngrx/effects';
import { storeUserAction } from '../actions/user.actions';
import { map } from 'rxjs/operators';
import { StorageMap } from '@ngx-pwa/local-storage';


@Injectable()
export class UserEffects {
  constructor(private actions$: Actions, private storage: StorageMap) {}

  
  setUser$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(storeUserAction),
        map(action => {
          this.storage.set('appuser', action.appuser).subscribe();
        })
      ),
    { dispatch: false }
  );



}



