import { AbstractClassPart } from '@angular/compiler/src/output/output_ast';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';

import { environment } from 'src/environments/environment';
import * as fromUser from './reducers/user.reducer';

export interface State {
  [fromUser.UserFeatureKey]: fromUser.UserState;
}

export const reducers: ActionReducerMap<State> = {
  [fromUser.UserFeatureKey]: fromUser.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
