import { Action, createReducer, on } from '@ngrx/store';
import { getUserAction, storeUserAction } from '../actions/user.actions';
import {User} from 'src/app/models/user'
export const UserFeatureKey = 'appuser';

export interface UserState {
  appuser: User;
}


export const initialState: UserState = {
  appuser: {id: 0, name:'', midname:'', lastname:''}
};


const userReducer = createReducer(
  initialState,
  on(storeUserAction, (state, {appuser}) => ({ ...state, appuser: appuser })),
  on(getUserAction, (state) => ({ ...state}))
  
);

export function reducer(state: UserState | undefined, action: Action) {
  return userReducer(state, action);
}


