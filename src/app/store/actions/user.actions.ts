
import { createAction, props } from '@ngrx/store';
import {User} from 'src/app/models/user'


export const storeUserAction = createAction(
  '[User] Store User',
  props<{ appuser: User }>()
);

export const getUserAction = createAction(
  '[User] Get User'
);

export const clearUserAction = createAction(
  '[User] Clear User'

);