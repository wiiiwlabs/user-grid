import { Component } from '@angular/core';

import DataSource from 'devextreme/data/data_source';
import LocalStore from 'devextreme/data/local_store';

import { AppStoreService, UserInitService } from 'src/app/service';
import { DatagridConfig } from './datagrid-config';

@Component({
  selector: 'app-user-datagrid',
  templateUrl: './user-datagrid.component.html',
  styleUrls: ['./user-datagrid.component.css'],
})
export class UserDataGridComponent {
  dgConfig: DatagridConfig;

  //настройка стора грида
  localstorage = new LocalStore({
    key: 'id',
    name: 'userlistjson',
    immediate: true,
  });

  //настройка DataSource
  dataSource = new DataSource({
    reshapeOnPush: true,
    store: this.localstorage,
  });

  //настройка toolbar
  onToolbarPreparing(e) {
    e.toolbarOptions.items[0].showText = 'always';
    e.toolbarOptions.items.push({
      location: 'before',
      template: 'FavoritesButton',
    });
  }

  constructor(private userService: UserInitService, private appStore: AppStoreService) {
    this.dgConfig = new DatagridConfig(this);
  }

  SetAppUser() {}
}
