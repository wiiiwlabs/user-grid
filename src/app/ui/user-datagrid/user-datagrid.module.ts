import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular/ui/button';

import { UserDataGridComponent } from './user-datagrid.component';

@NgModule({
  imports: [
    CommonModule,
    DxDataGridModule,
    DxButtonModule
    
  ],
  declarations: [UserDataGridComponent],
  exports: [ UserDataGridComponent ],
 
})
export class UserDataGridModule { }
