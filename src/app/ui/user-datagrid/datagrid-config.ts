import { User } from 'src/app/models';

export class DatagridConfig {
  constructor(private Parent) {}

  // событие на смену селекта в гриде
  selectionChanged(data: any) {
    this.saveUserOnAppStore(data); //Сохраним пользователя в АппСторе
  }

  // событие на чтение контента для грида
  dgContetnReady(e) {
    // если сторк в гриде меньше или равно 0 инициализируем строки из  StoreService
    if (e.component.totalCount() <= 0) this.reloadDataGridItem();
  }

  // Реализовать хранение текущего пользователя с использованием NgRx store.
  saveUserOnAppStore(data: any) {
    this.Parent.appStore.saveUser(<User>{ ...data.selectedRowsData[0] });
  }

  // Загрузка грида по умолчанию
  reloadDataGridItem() {
    let uitms: User[] = this.Parent.userService.getInitUsers();
    uitms.map((item) => this.Parent.localstorage.insert(item));
    this.Parent.dataSource.reload();
  }
}
