import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'src/app/models';

@Pipe({
  name: 'shortfio',
})
export class ShorFioPipe implements PipeTransform {
  transform(value: User, args?: any): string {
    if (typeof value !== 'object') return '';
    return `${value.lastname} ${value.name
      .charAt(0)
      .toUpperCase()}. ${value.midname.charAt(0).toUpperCase()}. `;
  }
}
