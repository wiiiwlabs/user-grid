import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './store';

import { loadMessages, locale } from 'devextreme/localization';
import * as ruMessages from 'devextreme/localization/messages/ru.json';

import { User } from 'src/app/models';
import { UserInitService, LocalStorageService, AppStoreService } from 'src/app/service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'user-grid';

  selecteduser: User;

  constructor(
    private store: Store<State>,
    private userService: UserInitService,
    private localStorage: LocalStorageService,
    private ngStore: AppStoreService
  ) {
    locale('ru');
    loadMessages(ruMessages);
  }

  ngOnInit() {
    this.store.select('appuser').subscribe((data) => {
      // если получили undefined или 0
      if (typeof data.appuser == 'undefined' || data.appuser.id == 0) {
        // пытаемся прочесть с LocalStorage
        this.localStorage.LocalStorageGet('appuser').subscribe((data) => {
          this.selecteduser = data;

          // если пусто то загужаем в стор из json
          if (typeof data == 'undefined') {
            let user: User = this.userService.getFirstUser();
            this.ngStore.saveUser(user);
            this.selecteduser = user;
          }
        });
      }

      this.selecteduser = data.appuser;
    });
  }
}
