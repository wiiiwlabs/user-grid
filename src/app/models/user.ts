export interface User {
    id: number;
    name: string;
    midname: string;
    lastname: string;
}

