# UserGrid 

**Задание:**

Стек: Angular, Angular Material, NgRx, RxJs, DexExtreme
Создать приложение с использованием: Angular версии 10+, Angular Material
10+, NgRx 10+, RxJS 6+, DevExtreem 20+
Цель создаваемого приложения - реализовать интерфейс работы со списком
пользователей.

* Реализовать сервис для получения списка пользователей. Сервис может
отдавать данные из обычного json объекта.
* Реализовать хранение текущего пользователя с использованием NgRx store.
При инициализации приложения, автоматически выбирать первого
пользователя из списка пользователей.
* Реализовать добавление, удаление, редактирование пользователей
* Реализовать компонент, содержащий DevExtreme DataGrid со списком
пользователей.
* Реализовать ленивую загрузку модуля, содержащего список пользователей.
Перед открытием компонента, ему должен быть доступен список
пользователей
* Реализовать возможность выбирать в списке пользователей любого
пользователя, и устанавливать его в качестве текущего пользователя
приложения. При изменении выбранного пользователя, изменять данные в
левом сайднаве
* Вынести всю бизнес-логику из компонента, содержащего список
пользователей в сервис, выступающий в качестве фасада.
* Использовать модульность. Каждая сущность является отдельным модулем
и в случае чего может перенестись на другой проект без больших трудозатрат
Дизайн и спорные вопросы на усмотрение разработчика


---


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
